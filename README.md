# FNMONITOR

This provides a web front end to the bacterial relatedness server findNeighbour3, available [here](https://github.com/davidhwyllie/findNeighbour3). 
FindNeighbour3 is a server application for investigating bacterial relatedness using reference-mapped data.
Accessible via RESTful webservices, findNeighbour3 maintains a sparse distance matrix in a database for a set of sequences.
It also maintains details of similar sequences (clusters).  This application is a web front end to findNeighbour3, and consumes its web services.  

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Deploy with a docker

From the project folder Run `ng build --prod` to build the project. The build artifacts will be stored in the `dist/FNMONITOR` directory

Go to the docker folder 

-   Run `docker image build -t yourDockerHubAcc/fnmonitor .` to build a docker image

-   Run `docker tag yourDockerHubAcc/fnmonitor:latest yourDockerHubAcc/fnmonitor:version`

-   Run `docker push yourDockerHubAcc/fnmonitor` to push the docker into your acc

## Run the app from a docker

-   Run `docker pull triendo/fnmonitor`

-   Run `docker run -d -p 3020:80 --rm triendo/fnmonitor`

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
