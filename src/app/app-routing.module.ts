import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FnmFindNeighbourComponent } from './fnm-find-neighbour/fnm-find-neighbour.component';
import { FnmReviewClusterComponent } from './fnm-review-cluster/fnm-review-cluster.component';
import { FnmBrowseClusterComponent } from './fnm-browse-cluster/fnm-browse-cluster.component';
import { FnmServerMonitorComponent } from './fnm-server-monitor/fnm-server-monitor.component';
import { FnmSampleAnnotationComponent } from './fnm-sample-annotation/fnm-sample-annotation.component';
import { FnmClusterDetailComponent } from './fnm-cluster-detail/fnm-cluster-detail.component';

const routes: Routes = [
  { 
    path: '',   
    redirectTo: '/fnmfindneighbour', 
    pathMatch: 'full' 
  },
  {
    path: 'fnmfindneighbour',
    component: FnmFindNeighbourComponent
  },
  {
    path: 'fnmsample/:guid',
    component: FnmSampleAnnotationComponent  
  },
  {
    path: 'fnmsample/:guid/clusters/:clusterMethod/:id',
    redirectTo: 'fnmreviewcluster/details/:guid/clusters/:clusterMethod/:id',
    pathMatch: 'full',  
  },
  {
    path: 'fnmfindneighbour/details/:guid',
    redirectTo: 'fnmsample/:guid',
    pathMatch: 'full',  
  },  
  {
    path: 'fnmreviewcluster',
    component: FnmReviewClusterComponent
  },
  {
    path: 'fnmreviewcluster/details/:guid',
    redirectTo: 'fnmsample/:guid',
    pathMatch: 'full',  
  },
  {
    path: 'fnmreviewcluster/details/:guid/clusters/:clusterMethod/:id',
    component: FnmReviewClusterComponent
  },
  {
    path: 'fnmreviewcluster/details/:guid/clusters/:clusterMethod/:id/details/:guid',
    redirectTo: 'fnmsample/:guid',
    pathMatch: 'full', 
  },
  {
    path: 'fnmbrowsecluster',
    component: FnmBrowseClusterComponent
  },
  {
    path: 'fnmbrowsecluster/details/:clusterMethod/:id',
    component: FnmClusterDetailComponent  
  },
  {
    path: 'fnmbrowsecluster/details/:clusterMethod/:id/details/:guid',
    redirectTo: 'fnmsample/:guid',
    pathMatch: 'full',  
  },
  {
    path: 'fnmservermonitor',
    component: FnmServerMonitorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
