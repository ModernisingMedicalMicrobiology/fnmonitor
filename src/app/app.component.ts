import { Component } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { FnmService } from './fnm.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {  
  title = '';
  constructor(private fnmService: FnmService){}

  ngOnInit() {
    var serverAddress = localStorage.getItem("appcomponent_serverAddress");
    var serverName = localStorage.getItem("appcomponent_serverName");
    if(isNullOrUndefined(serverAddress))
      this.changeServer();
    else{
      this.fnmService.setServerAddress(serverAddress);      
      this.title = this.getTitle(serverName,serverAddress);
    }
  }

  getTitle(serverName: string, serverAddress): string {
    return `Server name: ${serverName}; Address: ${serverAddress}`
  }

  changeServer() {
    var tmpAddress = localStorage.getItem("appcomponent_serverAddress");
    if(isNullOrUndefined(tmpAddress))
      tmpAddress = "http://IP_ADDRESS:PORT";
    var serverAddress = window.prompt(`Please type the address of the server (e.g. ${tmpAddress})`,tmpAddress);
    this.fnmService.validateServer(serverAddress).subscribe(
      (resp) => {
        var serverName = resp.server_name == undefined ? "" : resp.server_name;
        localStorage.setItem("appcomponent_serverAddress",serverAddress);
        localStorage.setItem("appcomponent_serverName",serverName);
        this.fnmService.setServerAddress(serverAddress);
        this.title = this.getTitle(serverName,serverAddress);       
      },
      (error) => {
        alert(`Error when varifying the FindNeighbour Server. The FindNeighbour server is either invalid or down. Please update the server address (ErrorCode: ${error.status})`);        
      }
    );    
  }  
}
