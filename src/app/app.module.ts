import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatAutocompleteModule, MatInputModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';//npm i @angular/cdk
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';//npm i @ng-bootstrap/ng-bootstrap

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FnmNavigationComponent } from './fnm-navigation/fnm-navigation.component';
import { FnmServerMonitorComponent } from './fnm-server-monitor/fnm-server-monitor.component';
import { FnmReviewClusterComponent } from './fnm-review-cluster/fnm-review-cluster.component';
import { FnmBrowseClusterComponent } from './fnm-browse-cluster/fnm-browse-cluster.component';
import { FnmFindNeighbourComponent } from './fnm-find-neighbour/fnm-find-neighbour.component';
import { FnmSampleAnnotationComponent } from './fnm-sample-annotation/fnm-sample-annotation.component';
import { FnmClusterDetailComponent } from './fnm-cluster-detail/fnm-cluster-detail.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    FnmNavigationComponent,
    FnmServerMonitorComponent,
    FnmReviewClusterComponent,
    FnmBrowseClusterComponent,
    FnmFindNeighbourComponent,
    FnmSampleAnnotationComponent,
    FnmClusterDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    LayoutModule,
    NgxSpinnerModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatTableModule, 
    MatPaginatorModule,
    MatSortModule, 
    MatFormFieldModule,  
    MatInputModule,  
    NgbModule.forRoot(),
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
