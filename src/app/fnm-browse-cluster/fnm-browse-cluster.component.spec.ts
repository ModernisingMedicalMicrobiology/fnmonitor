import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmBrowseClusterComponent } from './fnm-browse-cluster.component';

describe('FnmBrowseClusterComponent', () => {
  let component: FnmBrowseClusterComponent;
  let fixture: ComponentFixture<FnmBrowseClusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmBrowseClusterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmBrowseClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
