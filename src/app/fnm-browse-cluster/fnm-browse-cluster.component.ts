import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { FnmService } from '../fnm.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-fnm-browse-cluster',
  templateUrl: './fnm-browse-cluster.component.html',
  styleUrls: ['./fnm-browse-cluster.component.css']
})
export class FnmBrowseClusterComponent implements OnInit {
  clusterMethods = [];
  clusterMethod: string = "";
  clusters = [];
  displayedColumns: string[] = ['cluster_id','is_mixed_True','is_mixed_False','action'];  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  constructor(private fnmService: FnmService) { }

  ngOnInit() {    
    this.fnmService.getClusteringMethods().subscribe(      
      (resp) => {
        if(resp.algorithms!= undefined && resp.algorithms.length > 0){
          this.clusterMethods = resp.algorithms;
          this.clusterMethod = this.clusterMethods[0];
          this.restoreFormState();
        }
      },
      (error) =>{
        alert(`Error when querying clustering methods (ErrorCode: ${error.status})`);
      }
    );     
  }

  getClusters(){
    this.fnmService.getClusters(this.clusterMethod).subscribe(      
      (resp) => {
        this.clusters = resp.summary;
        this.dataSource = new MatTableDataSource<any>(this.clusters);  
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;   
        this.saveFormState();
      },
      (error) =>{
        alert(`Error when querying clustering methods (ErrorCode: ${error.status})`);
      }
    );    
  }

  saveFormState(){
    sessionStorage.setItem("fnmbrowsecluster_clusterMethod", this.clusterMethod);
  }

  restoreFormState(){
    var cMethod = sessionStorage.getItem("fnmbrowsecluster_clusterMethod");
    if(!isNullOrUndefined(cMethod)){
      this.clusterMethod = cMethod;      
      this.getClusters();
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
