import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmClusterDetailComponent } from './fnm-cluster-detail.component';

describe('FnmClusterDetailComponent', () => {
  let component: FnmClusterDetailComponent;
  let fixture: ComponentFixture<FnmClusterDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmClusterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmClusterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
