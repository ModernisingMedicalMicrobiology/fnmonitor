import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FnmService } from '../fnm.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fnm-cluster-detail',
  templateUrl: './fnm-cluster-detail.component.html',
  styleUrls: ['./fnm-cluster-detail.component.css']
})
export class FnmClusterDetailComponent implements OnInit {
  clusterId: number = 0;
  displayedColumns: string[] = ['guid', 'change_id', 'is_mixed', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: any;
  constructor(private router:Router, private activeRoute: ActivatedRoute, private fnmService: FnmService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.clusterId = params["id"];
      var clusteringMethod = params["clusterMethod"];
      if (this.clusterId && clusteringMethod) {
        this.fnmService.getClusterMembers(clusteringMethod,this.clusterId).subscribe(
          (resp) => {            
            this.dataSource = new MatTableDataSource<any>(resp.members);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          (error) => {
            alert(`Error when querying members of a cluster (ErrorCode: ${error.status})`);
          }
        );
      }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
