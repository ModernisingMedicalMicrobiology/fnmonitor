import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmFindNeighbourComponent } from './fnm-find-neighbour.component';

describe('FnmFindNeighbourComponent', () => {
  let component: FnmFindNeighbourComponent;
  let fixture: ComponentFixture<FnmFindNeighbourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmFindNeighbourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmFindNeighbourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
