import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FnmService } from '../fnm.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, single } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-fnm-find-neighbour',
  templateUrl: './fnm-find-neighbour.component.html',
  styleUrls: ['./fnm-find-neighbour.component.css']
})
export class FnmFindNeighbourComponent implements OnInit {
  displayedColumns: string[] = ['guid', 'snv', 'action'];
  sampleId: string = "";
  statusText: string = "Please enter a sample Id then click Find";
  threshold: number = 20;
  thresholdMax: number = 20;//Get from Rest API
  dataSource: any;
  neighbours = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  //For filter sample Id
  sampleIdControl = new FormControl();
  allSampleIds = [];
  filteredOptions: Observable<string[]>;

  constructor(private fnmService: FnmService) { }

  ngOnInit() {
    //Get max SNP
    this.fnmService.getSnpCeiling().subscribe(
      (resp) => {
        this.thresholdMax = resp.snpceiling;
        this.restoreFormState()
      },
      (error) => {
        alert(`Error when getting SNP ceiling (ErrorCode: ${error.status})`);
        this.neighbours = [];
      }
    );
  }

  onSampleIdChange(sampleId: string) {
    this.statusText = "Please enter a sample Id then click Find";
    this.neighbours = [];
    this.setupTable([]);
    if (sampleId.trim() == "") {
      this.allSampleIds = [];
      this.filteredOptions = this.sampleIdControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      return;
    }
    this.fnmService.getAllSampleStartWith(sampleId).subscribe(
      (resp) => {
        this.allSampleIds = resp;
        this.filteredOptions = this.sampleIdControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      },
      (error) => {
        alert(`Error when filtering sample Ids (ErrorCode: ${error.status})`);
        this.neighbours = [];
      }
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allSampleIds.filter(option => option.toLowerCase().includes(filterValue));
  }

  findNeighbours() {
    this.sampleId = this.sampleId.trim();
    if (this.sampleId.length == 0)
      alert('Sample Id cannot be empty!');
    else
      this.fnmService.getNeighbours(this.sampleId, this.threshold).subscribe(
        (resp) => {
          if (resp.error) {
            alert('The sample Id may not exist')
            this.neighbours = [];
          }
          else {
            this.neighbours = resp;
            this.statusText = `Found ${this.neighbours.length} neighbours for sample ${this.sampleId}`;
            this.saveFormState();
            this.setupTable(resp);
          }
        },
        (error) => {
          alert(`The entered Sample Id does not exist (ErrorCode: ${error.status})`);
          this.neighbours = [];
        }
      );
  }

  saveFormState(){
    sessionStorage.setItem("fnmfindneighbour_sampleId", this.sampleId);
    sessionStorage.setItem("fnmfindneighbour_threshold", this.threshold.toString());
  }

  restoreFormState(){
    var sId = sessionStorage.getItem("fnmfindneighbour_sampleId");
    var thold = sessionStorage.getItem("fnmfindneighbour_threshold");
    if(!isNullOrUndefined(sId) && !isNullOrUndefined(thold)){
      this.sampleId = sId;
      this.threshold = parseInt(thold);
      this.findNeighbours();
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setupTable(data: any){
    this.dataSource = new MatTableDataSource<any>(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
