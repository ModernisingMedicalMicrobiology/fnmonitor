import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmNavigationComponent } from './fnm-navigation.component';

describe('FnmNavigationComponent', () => {
  let component: FnmNavigationComponent;
  let fixture: ComponentFixture<FnmNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
