import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmReviewClusterComponent } from './fnm-review-cluster.component';

describe('FnmReviewClusterComponent', () => {
  let component: FnmReviewClusterComponent;
  let fixture: ComponentFixture<FnmReviewClusterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmReviewClusterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmReviewClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
