import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FnmService } from '../fnm.service';
import * as $ from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
import * as XLSX from 'xlsx';
//http://js.cytoscape.org/#getting-started/including-cytoscape.js
declare var require: any;//Just for creating production

@Component({
  selector: 'app-fnm-review-cluster',
  templateUrl: './fnm-review-cluster.component.html',
  styleUrls: ['./fnm-review-cluster.component.css']
})
export class FnmReviewClusterComponent implements OnInit {
  displayedColumns: string[] = ['guid', 'aligned_seq_len', 'alignN', 'allN', 'alignM', 'allM', 'alignN_or_M', 'allN_or_M',
    'expected_proportion1','expected_proportion2', 'expected_proportion3', 'expected_proportion4', 'what_tested', 
    'observed_proportion', 'p_value1','p_value2', 'p_value3', 'p_value4', 'aligned_seq', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('TABLE') dataTable: ElementRef;
  dataSource: any;
  clusterMethods = [];
  clusterMethod: string = "";
  clusters = [];
  cluster: any;
  formats = ["json-records", "json-fasta", "network", "minimum_spanning_tree"];  
  format: string = "";
  fasta: string = "";
  alignmentCluster = [];
  showAlignedSeq:boolean = true;
  showTableOptions: boolean = false;
  prompt = "Mouseover to see Ids of nodes and distance between them";
  cytoscape = require('cytoscape');
  constructor(private fnmService: FnmService, private spinner: NgxSpinnerService, private router:Router, private activeRoute: ActivatedRoute) { }
  isFirstTimeLoadingCluster = true;
  ngOnInit() {
    this.format = this.formats[0];
    this.fnmService.getClusteringMethods().subscribe(
      (resp) => {
        if (resp.algorithms != undefined && resp.algorithms.length > 0) {
          this.clusterMethods = resp.algorithms;
          this.clusterMethod = this.clusterMethods[0];
          this.getCluster();
        }
      },
      (error) => {
        alert(`Error when querying clustering methods (ErrorCode: ${error.status})`);
      }
    );
  }

  renderFloatNumber(input){
    var nullValueRepresentation = "";
    return input == undefined ? nullValueRepresentation : input.toFixed(4);
  }
  exportToExcel() {
    const worksheet: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.dataTable.nativeElement);
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');    
    /* save to file */
    XLSX.writeFile(workbook, `cluster_${this.clusterMethod}_${this.cluster}.xlsx`);    
  }

  saveFormState(){
    sessionStorage.setItem("fnmreviewcluster_clusterMethod", this.clusterMethod);
    sessionStorage.setItem("fnmreviewcluster_cluster", this.cluster);
    sessionStorage.setItem("fnmreviewcluster_format", this.format);
  }

  restoreFormState(){
    var mMethod = sessionStorage.getItem("fnmreviewcluster_clusterMethod");
    var mCluster = sessionStorage.getItem("fnmreviewcluster_cluster");
    var mFormat = sessionStorage.getItem("fnmreviewcluster_format");
    if(!isNullOrUndefined(mMethod) && !isNullOrUndefined(mCluster) && !isNullOrUndefined(mFormat)){
      this.clusterMethod = mMethod;
      this.cluster = mCluster;
      this.format = mFormat;
      this.getAlignmentCluster();
    }
  }

  getCluster() {
    this.fnmService.getClusters(this.clusterMethod).subscribe(
      (resp) => {
        this.clusters = resp.summary;
        if (this.clusters.length > 0) {
          this.cluster = this.clusters[0].cluster_id;
          this.checkRedirectFromSampleDetails();
        }
      },
      (error) => {
        alert(`Error when querying cluster (ErrorCode: ${error.status})`);
      }
    );
  }

  checkRedirectFromSampleDetails(){
    //if direct -> there are two params: clustermethod and clusterId -> display info
    this.activeRoute.params.subscribe(params => {
      const id = params["id"];
      const clusterMethod = params["clusterMethod"];
      if(id && clusterMethod){
        this.clusterMethod = clusterMethod;
        this.cluster = id;
        this.getAlignmentCluster();
      }
      else if(this.isFirstTimeLoadingCluster){
        this.restoreFormState();
        this.isFirstTimeLoadingCluster = false;
      }
    });      
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  renderAlignedSequence(){
    if(this.alignmentCluster.length <=0 )
      return;
    if(this.showAlignedSeq){
      this.displayedColumns.splice(this.displayedColumns.length-1, 0, 'aligned_seq');      
    }
    else{
      this.displayedColumns.splice(this.displayedColumns.length-2, 1);      
    }
  }

  //Import jquery https://medium.com/@swarnakishore/how-to-include-and-use-jquery-in-angular-cli-project-592e0fe63176 NOTE Angular 6 uses angular.json instead angular-cli.json
  getAlignmentCluster() {    
    if (this.clusterMethod == undefined || this.clusterMethod.trim().length == 0 || this.cluster == undefined) {
      alert('Cluster method and cluster cannot be empty');
      return;
    }
    this.spinner.show();
    this.fnmService.getMultipleAlignmentCluster(this.clusterMethod, this.cluster, this.format).subscribe(
      (resp) => {
        if (this.format == this.formats[0]) {
          this.alignmentCluster = resp;
          this.dataSource = new MatTableDataSource<any>(resp);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.showTableOptions = this.alignmentCluster.length > 0 ;
          this.spinner.hide();
        }
        else if (this.format == this.formats[1]) {
          this.fasta = resp.fasta;
          this.fasta = this.fasta.replace(/\n/g, '<br/>')
          this.spinner.hide();
        }
        else {                    
          var canvas = $('#cy');
          this.generateCytoscapeObject(canvas, resp.elements);          
        }
        this.saveFormState();
      },
      (error) => {
        this.spinner.hide();
        alert(`Error when querying Alignment Cluster (ErrorCode: ${error.status})`);
      }
    );
  }

  generateCytoscapeObject(container, data) {
    var cmd = this.prompt;
    var spinnerInner = this.spinner;
    var custom_style = [{
      selector: 'node[is_mixed=0]',
      style: {
        'background-color': 'blue'
      }
    }, {
      selector: 'node[is_mixed=1]',
      style: {

        'background-color': 'red'
      }
    }, {
      selector: 'node[picked=1]',
      style: {
        'width': '5px',
        'height': '5px',
        'background-fit': 'contain',
        'background-clip': 'none'
      },

    }, {
      selector: 'node[picked=0]',
      style: {
        'shape': 'diamond',
        'width': '5px',
        'height': '5px',
        'background-fit': 'contain',
        'background-clip': 'none'
      },
    }, {
      selector: 'edge',
      style: {
        'text-background-color': 'yellow',
        'text-background-opacity': 0.4,
        'width': '0.5px',
        'target-arrow-shape': 'triangle',
        'control-point-step-size': '140px'
      }
    }
    ];

    var options = {
      name: 'cose',

      // Called on `layoutready`
      ready: function () {
      },

      // Called on `layoutstop`
      stop: function () { 
        spinnerInner.hide();
      },

      // Whether to animate while running the layout
      // true : Animate continuously as the layout is running
      // false : Just show the end result
      // 'end' : Animate with the end result, from the initial positions to the end positions
      animate: true,

      // Easing of the animation for animate:'end'
      animationEasing: undefined,

      // The duration of the animation for animate:'end'
      animationDuration: undefined,

      // A function that determines whether the node should be animated
      // All nodes animated by default on animate enabled
      // Non-animated nodes are positioned immediately when the layout starts
      animateFilter: function (node, i) {
        return true;
      },


      // The layout animates only after this many milliseconds for animate:true
      // (prevents flashing on fast runs)
      animationThreshold: 250,

      // Number of iterations between consecutive screen positions update
      // (0 -> only updated on the end)
      refresh: 20,

      // Whether to fit the network view after when done
      fit: true,

      // Padding on fit
      padding: 30,

      // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      boundingBox: undefined,

      // Excludes the label when calculating node bounding boxes for the layout algorithm
      nodeDimensionsIncludeLabels: false,

      // Randomize the initial positions of the nodes (true) or use existing positions (false)
      randomize: false,

      // Extra spacing between components in non-compound graphs
      componentSpacing: 40,

      // Node repulsion (non overlapping) multiplier
      nodeRepulsion: function (node) {
        return 2048;
      },

      // Node repulsion (overlapping) multiplier
      nodeOverlap: 4,

      // Ideal edge (non nested) length
      idealEdgeLength: function (edge) {
        return edge.data().snv + 0.5;
      },

      // Divisor to compute edge forces
      edgeElasticity: function (edge) {
        return 10;
        //return Math.sqrt(edge.data().snv+1);
      },

      // Nesting factor (multiplier) to compute ideal edge length for nested edges
      nestingFactor: 1.2,

      // Gravity force (constant)
      gravity: 1,

      // Maximum number of iterations to perform
      numIter: 1000,

      // Initial temperature (maximum node displacement)
      initialTemp: 1000,

      // Cooling factor (how the temperature is reduced between consecutive iterations
      coolingFactor: 0.99,

      // Lower temperature threshold (below this point the layout will end)
      minTemp: 1.0,

      // Pass a reference to weaver to use threads for calculations
      weaver: false
    };

    container.html("");
    var cy = this.cytoscape({
      container: container,
      elements: data,
      style: custom_style,
      layout: options
    });   
    cy.$("node").data("picked", 0);  // nothing selected
    cy.on('mouseover', 'node', function (event) {
      document.getElementById('recentNode').innerHTML = event.target.id();
    });
    cy.on('mouseout', 'node', function (event) {
      document.getElementById('recentNode').innerHTML = cmd;
    });
    cy.on('mouseover', 'edge', function (event) {
      // var node = event.cyTarget;

      var id = event.target.id();
      var edata = cy.$("#" + id).data();
      var edescr = edata.snv + ' snv [' + edata.source + '-----' + edata.target + ']';
      document.getElementById('recentNode').innerHTML = edescr;
    });
    cy.on('mouseout', 'edge', function (event) {
      document.getElementById('recentNode').innerHTML = cmd;
    });
    cy.on('click', 'node', function (event) {
      // var node = event.cyTarget;
      var id = event.target.id();
      if (cy.$("#" + id).data("picked") == 0) {
        cy.$("#" + id).data("picked", 1);
        document.getElementById('recentNode').innerHTML = event.target.id() + " Selected";
      } else {
        cy.$("#" + id).data("picked", 0);
        document.getElementById('recentNode').innerHTML = event.target.id() + " Deselected";
      }
    });
  }
}

