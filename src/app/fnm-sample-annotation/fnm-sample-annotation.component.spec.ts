import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmSampleAnnotationComponent } from './fnm-sample-annotation.component';

describe('FnmSampleAnnotationComponent', () => {
  let component: FnmSampleAnnotationComponent;
  let fixture: ComponentFixture<FnmSampleAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmSampleAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmSampleAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
