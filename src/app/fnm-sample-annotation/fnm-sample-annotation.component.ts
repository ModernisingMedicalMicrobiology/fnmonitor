import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FnmService } from '../fnm.service';

@Component({
  selector: 'app-fnm-sample-annotation',
  templateUrl: './fnm-sample-annotation.component.html',
  styleUrls: ['./fnm-sample-annotation.component.css']
})
export class FnmSampleAnnotationComponent implements OnInit {
  annotations: any;
  clusters: any;

  constructor(private router:Router, private activeRoute: ActivatedRoute, private fnmService: FnmService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      const id = params["guid"];
      if(id){
        this.fnmService.getSampleAnnotation(id).subscribe(
          (resp:any) => {
            if (resp) {
              this.annotations = resp
            } 
            else {
              alert(`Sample ${id} not found`);              
            }
          },
          (error) => {                
            alert(`Error when loading annotation for sample ${id}. ${error.error.message}`);
          }
        );
        this.fnmService.getClustersForSample(id).subscribe(
          (resp:any) => {
            if (resp) {
              this.clusters = resp
            } 
            else {
              alert(`Sample ${id} not found`);              
            }
          },
          (error) => {                
            alert(`Error when loading annotation for sample ${id}. ${error.error.message}`);
          }
        );
      }
      else{
        alert("No sample Id provided");
      }
    });
  }
}
