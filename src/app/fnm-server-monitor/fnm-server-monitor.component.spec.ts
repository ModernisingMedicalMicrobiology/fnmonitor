import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FnmServerMonitorComponent } from './fnm-server-monitor.component';

describe('FnmServerMonitorComponent', () => {
  let component: FnmServerMonitorComponent;
  let fixture: ComponentFixture<FnmServerMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FnmServerMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FnmServerMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
