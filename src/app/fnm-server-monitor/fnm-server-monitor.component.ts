import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { StatType } from '../fnm.models';
import { FnmService } from '../fnm.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-fnm-server-monitor',
  templateUrl: './fnm-server-monitor.component.html',
  styleUrls: ['./fnm-server-monitor.component.css']
})
export class FnmServerMonitorComponent implements OnInit {
  statTypes: StatType[] = [ new StatType('Memory use', 'mstat'), new StatType('In memory sequences','scstat'), 
                                    new StatType('Guid2metadata database size','guid2meta'), new StatType('Guid2neighbour database size','guid2neighbour' ),
                                    new StatType('Compressed sequence database size','refcompressedseq'), new StatType('Clusters database size', 'clusters'),
                                    new StatType('Server monitoring database size','server')];
  statValue: string;
  eventRanges: number[] = [10,20,50, 100, 200, 500, 1000];
  eventRange: number;
  serverStatus: any;
  @ViewChild("trendsImage") trendsImage: ElementRef;
  _window: Window;
  interverPeriod = 1*60*1000;//Every minute = 1 * 60 * 1000ms
  intervalId = null;

  constructor(private fnmService: FnmService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.statValue = this.statTypes[0].value;
    this.eventRange = this.eventRanges[0];
    this._window = window;
    this.getStatus();
    this.getTrends();    
  }

  //Using wating spinner https://www.npmjs.com/package/ngx-spinner
  getTrends(isFirstimme = true){
    if(isFirstimme)
      this.spinner.show();
    this.fnmService.getTrends(this.statValue,this.eventRange).subscribe(      
      (resp) => {
        var urlCreator = this._window.URL;
        this.trendsImage.nativeElement.src = urlCreator.createObjectURL(resp);
        this.spinner.hide();
        if(isFirstimme)
          this.autoRefresh();
      },
      (error) =>{
        this.spinner.hide();
        alert(`Error when searching samples (ErrorCode: ${error.status})`);
      }
    );     
  }

  getStatus(){
    this.fnmService.getStatus().subscribe(      
      (resp) => {
        this.serverStatus = resp;
      },
      (error) =>{
        alert(`Error when searching samples (ErrorCode: ${error.status})`);
      }
    );     
  }

  autoRefresh(){
    clearInterval(this.intervalId);
    this.intervalId = setInterval(() => {
        this.getStatus();
        this.getTrends(false);
      }, 
      this.interverPeriod);
  }
}
