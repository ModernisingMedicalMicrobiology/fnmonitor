export class StatType  {
    constructor(        
        public text: string,
        public value: string
    )
    {}
}