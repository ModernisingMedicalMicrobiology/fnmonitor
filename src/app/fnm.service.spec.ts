import { TestBed } from '@angular/core/testing';

import { FnmService } from './fnm.service';

describe('FnmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FnmService = TestBed.get(FnmService);
    expect(service).toBeTruthy();
  });
});
