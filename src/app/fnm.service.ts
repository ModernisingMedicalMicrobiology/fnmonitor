import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
//import { NcbiProject, MmmUser, MmmAssignment, MmmSubmission, MmmSample } from './mmm2ncbi.models';
@Injectable({
  providedIn: 'root'
})
export class FnmService {
  private API_ROOT = "";
  constructor(private http: HttpClient) { }
  setServerAddress(address: string){
    this.API_ROOT = address;
  }
  //Check if the input address is an Findneighbour Server
  validateServer(address:string){
    const url = `${address}/api/v2/server_name`;       
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("validateServer"))
    );    
  }

  getSampleAnnotation(sampleId: string): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/${sampleId}/annotation`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getSampleAnnotation"))
    );     
  }

  getSnpCeiling(){
    const url = `${this.API_ROOT}/api/v2/snpceiling`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getSnpCeiling"))
    );    
  }

  getMultipleAlignmentCluster(clusteringMethod: string, clusterId: string, format: string){
    var apiPart = "clustering";
    if(format == "json-records" || format == "json-fasta")
      apiPart = "multiple_alignment_cluster";
    const url = `${this.API_ROOT}/api/v2/${apiPart}/${clusteringMethod}/${clusterId}/${format}`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getMultipleAlignmentCluster"))
    );    
  
  }

  getClusters(clusteringMethod: string): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/clustering/${clusteringMethod}/clusters`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getClusters"))
    );     
  }

  getClustersForSample(sampleId: string): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/${sampleId}/clusters`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getClustersForSample"))
    );     
  }

  getClusterMembers(clusteringMethod: string, clusterId: number): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/clustering/${clusteringMethod}/${clusterId}`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getClusterMembers"))
    );     
  }

  getClusteringMethods(): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/clustering`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getClusteringMethods"))
    );     
  }

  getAllSampleIds(): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/guids`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getAllSampleIds"))
    );     
  }

  getAllSampleStartWith(startId: string): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/guids_beginning_with/${startId}`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getAllSampleIds"))
    );     
  }

  getNeighbours(sampleId: string, threshold: number): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/${sampleId}/neighbours_within/${threshold}/in_format/4`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<any>("getNeighbours"))
    );     
  }

  getTrends(statType: string, range: number): Observable<Blob> {    
    const url = `${this.API_ROOT}/ui/server_status/absolute/${statType}/${range}`;   
    const options = {responseType: "blob" as "json"};
    return this.http.get<Blob>(url, options).pipe(
      retry(3),      
      catchError(this.handleError<Blob>("getTrends"))
    );     
  }

  getStatus(): Observable<any> {    
    const url = `${this.API_ROOT}/api/v2/server_memory_usage/1`;   
    return this.http.get<any>(url).pipe(
      retry(3),      
      catchError(this.handleError<Blob>("getStatus"))
    );     
  }

  /**
   * Handle Http operation that failed.   
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      this.log(error); 
      // TODO: better job of transforming error for user consumption
      if(error)//Check this later
        this.log(`Error message: ${operation} failed: ${error.message}`);
      return throwError(error);
    };
  }

  /** Log error message with a MessageService if neccesary */
  private log(message: string) {
    console.error(message);
    //this.messageService.add(`Mmm2NcbiService: ${message}`);
  } 
}
